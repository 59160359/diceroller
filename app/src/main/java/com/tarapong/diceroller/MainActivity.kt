package com.tarapong.diceroller

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
//    var diceImage: ImageView? = null
    private lateinit var diceImage: ImageView
    lateinit var rollButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val rollbutton: Button = findViewById<Button>(R.id.roll_button)
        rollButton = findViewById<Button>(R.id.roll_button)
        diceImage = findViewById(R.id.dice_image)
        rollButton.setOnClickListener{rollDice()}
    }
    private fun rollDice(){
//        Toast.makeText(this, "Button Clicked", Toast.LENGTH_SHORT).show()
//        val resultText: TextView = findViewById(R.id.result_text)
//        val randomInt = (1..6).random()
////        resultText.text = "Dice Rolled!"
////        resultText.text = randomInt.toString()
//        val diceImage: ImageView = findViewById(R.id.dice_image)
//        val drawableResult = when(randomInt){
        val drawableResult = when((1..6).random()){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            6 -> R.drawable.dice_6
            else -> R.drawable.empty_dice
        }
//        diceImage?.setImageResource(drawableResult)
        diceImage.setImageResource(drawableResult)
    }
}